// app.js
const express = require('express');
const app = express();
const port = 80;

// Функция для форматирования времени в формате HH:MM:SS
function formatTime(date) {
const hours = String(date.getHours()).padStart(2, '0');
const minutes = String(date.getMinutes()).padStart(2, '0');
const seconds = String(date.getSeconds()).padStart(2, '0');
return `${hours}:${minutes}:${seconds}`;
}

// Обработчик GET-запроса к корневому URL
app.get('/', (req, res) => {
const currentTime = formatTime(new Date()); // Получаем текущее время сервера
res.send(`Hello World!!!!! Current server time is ${currentTime}`);
});

// Запуск приложения на указанном порту
app.listen(port, () => console.log(`App listening at http://localhost:${port}`));